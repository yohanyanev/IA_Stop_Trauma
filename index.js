//FONCTION MAGIC LINE
$(function() {
    var $el,
      leftPos,
      newWidth,
      $mainNav = $(".navbar-nav");
  
    $mainNav.append("<li id='magic-line'></li>");
    var $magicLine = $("#magic-line");
  
    $magicLine
      .width($(".active").width())
      .css("left", $(".active a").position().left)
      .data("origLeft", $magicLine.position().left)
      .data("origWidth", $magicLine.width());
  
    $(".navbar-nav li a").hover(
      function() {
        $el = $(this);
        leftPos = $el.position().left;
        newWidth = $el.parent().width();
        $magicLine.stop().animate({
          left: leftPos,
          width: newWidth
        });
      },
      function() {
        $magicLine.stop().animate({
          left: $magicLine.data("origLeft"),
          width: $magicLine.data("origWidth")
        });
      }
    );
  });

  //SAVE A TEXT INTO FILE
  function save (filename,text){
    // Set up the link
    var link = document.createElement("a");
    link.setAttribute("target","_blank");
    if(Blob !== undefined) {
        var blob = new Blob([text], {type: "text/plain"});
        link.setAttribute("href", URL.createObjectURL(blob));
    } else {
        link.setAttribute("href","data:text/plain," + encodeURIComponent(text));
    }
    link.setAttribute("download",filename);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
  
//COUNT ROW WITH SPECFIC VALUE
function rowsWithValue(params) {
    if (!params && params.length < 2) 
        return false;
    var matrixValue = params[0];
    var value = params[1];
    var rowCount = 0;
    for (var key in matrixValue) {
        if (matrixValue[key] == value) 
            rowCount++;
        }
    return rowCount;
}

//COLORS MARIA
// var defaultThemeColors = Survey
//     .StylesManager
//     .ThemeColors["default"];
// defaultThemeColors["$main-color"] = "#004c40";
// defaultThemeColors["$main-hover-color"] = "#e0e0e0";
// defaultThemeColors["$text-color"] = "#616161";
// defaultThemeColors["$header-color"] = "#ffffff";

// defaultThemeColors["$header-background-color"] = "#00796B";
// defaultThemeColors["$body-container-background-color"] = "#f8f8f8";
//MARIA CAN CHANGE COLORS HERE

Survey
    .FunctionFactory
    .Instance
    .register("rowsWithValue", rowsWithValue);

Survey
    .StylesManager
    .applyTheme("default");

    
var json = {
    title: "Stop Trauma",
    locale: "fr",
    showProgressBar: "bottom",
    goNextPageAutomatic: true,
    showNavigationButtons: false,
    pages: [
        {
            questions: [
                {
                    name: "Nom",
                    type: "text",
                    title: "Nom:",
                    placeHolder: "SMITH",
                    isRequired: true
                }, {
                    name: "Prenom",
                    type: "text",
                    inputType: "text",
                    placeHolder: "Jon",
                    title: "Prenom:",
                    isRequired: true
                },{
                    name: "age",
                    type: "date",
                    title: "Age:",
                    isRequired: true
                }, {
                    name: "profession",
                    type: "text",
                    inputType: "text",
                    title: "Profession:",
                    isRequired: true
                } 
                
            ]//***************************************************PAGE0 */
        },{
            questions: [
                {
                    type: "dropdown",
                    name: "probleme",
                    title: "{Nom} ,choisissez votre probleme?",
                    isRequired: true,
                    choices: [
                        "Peurs / Angoisse",
                        "Douleurs" ,
                        "Stress / Fatigue / Troubles de l’attention",
                        "Traumatismes / Séparation / Deuil",
                        "Dépression / Solitude",
                        "Manque de confiance",
                        "Addiction / Tabac",
                        "Troubles alimentaires / Perte de poids / Troubles digestif",
                        "Troubles du sommeil",
                        "Troubles cutanés",
                        "Phobies"
                    ],
                    colCount: 0
                },{
                    type: "dropdown",
                    name: "depuis temps",
                    title: "Depuis combien de temps date ton problème ?",
                    isRequired: true,
                    choices: [
                        "0 a 5 ans",
                        "5 a 10 ans" ,
                        "10 a 15 ans",
                        "15 a 20 ans",
                        "plus de 25 ans"
                    ],
                    colCount: 0
                },{
                    type: "dropdown",
                    name: "A l'age de",
                    title: "Quel âge avais-tu à ce moment-là ?",
                    isRequired: true,
                    choices: [
                        "0 a 5 ans",
                        "5 a 10 ans" ,
                        "10 a 15 ans",
                        "15 a 20 ans",
                        "plus de 25 ans"
                    ],
                    colCount: 0
                }
            ]//****************************************************************PAGE 2
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "En couple",
                    hasOther: true,
                    title: "Es-tu en couple ?",
                    isRequired: true,
                    choices: ["oui","non"]
                },{
                    type: "text",
                    visible: "false",
                    name: "name Gf Bf",
                    title: "Nomes:",
                    isRequired: true
                },{
                    type: "matrix",
                    name: "qualite relation",
                    visibleIf:"{En couple} = 'oui'" ,
                    title: "Si tu devais évaluer la qualité de ta relation amoureuse à ce \
                            moment-là sur 10, en sachant que 0 = relation toxique et 10 = relation parfaite,\
                            combien lui mettrais-tu ? ",
                    columns: [
                        {
                            value: 0,
                        }, {
                            value: 1,
                        }, {
                            value: 3,
                        }, {
                            value: 4,
                        }, {
                            value: 5,
                        },{
                            value: 6,
                        },{
                            value: 7,
                        },{
                            value: 8,
                        },{
                            value: 9,
                        },{
                            value: 10,
                        }
                    ]
                    
                },{
                    type: "radiogroup",
                    name: "Pas en relation",
                    hasOther: true,
                    title: "Si non, étais-tu dans une période de séparation ou de divorce ?",
                    visibleIf: "{En couple} = 'non'",
                    isRequired: true,
                    choices: ["oui","non"]
                },{
                    type: "text",
                    name: "nome personne divorce/separation",
                    title: "Prenoms:",
                    visibleIf: "{Pas en relation} = 'oui'",
                    isRequired: true
                },{
                    type: "matrix",
                    name: "vivre celibat",
                    visibleIf:"{Pas en relation} = 'non'",
                    title: "Si non, comment vivais-tu ton célibat ?",
                    columns: [
                        {
                            value: "bien",
                            text: "bien",
                        }, {
                            value: "plutot bien",
                            text: "plutot bien"
                        }, {
                            value: "plutot mal",
                            text: "plutot mal"
                        }, {
                            value: "mal",
                            text: "mal" 
                        }
                    ]   
                }
            ]///*****************************************************************************PAGE3
        }, {
            questions: [
                {
                    type: "matrix",
                    name: "Appele son pere",
                    title: "Comment appelles-tu ton père ?",
                    columns: [
                        {
                            value: "dad",
                            text: "dad",
                        }, {
                            value: "daddy",
                            text: "daddy"
                        }, {
                            value: "papa",
                            text: "papa"
                        }, {
                            value: "je ne le conait pas",
                            text: "je ne le conait pas" 
                        }, {
                            value: "il est mort",
                            text: "il est mort" 
                        }
                    ]   
                },{
                    type: "radiogroup",
                    name: "En paix avec ca",
                    title: "Si tu ne l’as jamais connu, es-tu en paix avec cela ? ",
                    visibleIf: "{Appele son pere} = 'je ne le conait pas' or {Appele son pere} = 'il est mort' ",
                    isRequired: true,
                    choices: ["oui","non"]
                },{
                    type: "matrix",
                    name: "qualite relation père",
                    title: "Si tu devais évaluer la qualité de ta relation avec ton père entre 0 et 10, \
                            0 étant catastrophique et 10 étant excellente, quelle note mettrais-tu à cette relation ?",
                    columns: [
                        {
                            value: 0,
                        }, {
                            value: 1,
                        }, {
                            value: 3,
                        }, {
                            value: 4,
                        }, {
                            value: 5,
                        },{
                            value: 6,
                        },{
                            value: 7,
                        },{
                            value: 8,
                        },{
                            value: 9,
                        },{
                            value: 10,
                        }
                    ]
                    
                },{
                    type: "matrix",
                    name: "note son pere",
                    title: "Entre 0 et 10, en sachant que 0 est nul et que 10 est parfait.\
                             Quelle note mettez-vous au comportement de votre père par rapport à vous ? ",
                    columns: [
                        {
                            value: 0,
                        }, {
                            value: 1,
                        }, {
                            value: 3,
                        }, {
                            value: 4,
                        }, {
                            value: 5,
                        },{
                            value: 6,
                        },{
                            value: 7,
                        },{
                            value: 8,
                        },{
                            value: 9,
                        },{
                            value: 10,
                        }
                    ]
                    
                }
            ]///*******************************************************PAGE 4 MERE
        },{
            questions: [
                {
                    type: "matrix",
                    name: "Appele sa mere",
                    title: "Comment appelles-tu ta mère ?",
                    columns: [
                        {
                            value: "maman",
                            text: "maman",
                        }, {
                            value: "mere",
                            text: "mere"
                        }, {
                            value: "mom",
                            text: "mom"
                        }, {
                            value: "je ne la conait pas",
                            text: "je ne la conait pas" 
                        }, {
                            value: "elle est mort",
                            text: "elle est mort" 
                        }
                    ]   
                },{
                    type: "radiogroup",
                    name: "En paix avec ca",
                    title: "Si tu ne l’as jamais connue, es-tu en paix avec cela ? ",
                    visibleIf: "{Appele sa mere} = 'je ne la conait pas' or {Appele sa mere} = 'elle est mort' ",
                    isRequired: true,
                    choices: ["oui","non"]
                },{
                    type: "matrix",
                    name: "qualite relation mere",
                    title: "Si tu devais évaluer la qualité de ta relation avec ta mère entre 0 et 10, \
                            0 étant catastrophique et 10 étant excellente, quelle note mettrais-tu à cette relation ?",
                    columns: [
                        {
                            value: 0,
                        }, {
                            value: 1,
                        }, {
                            value: 3,
                        }, {
                            value: 4,
                        }, {
                            value: 5,
                        },{
                            value: 6,
                        },{
                            value: 7,
                        },{
                            value: 8,
                        },{
                            value: 9,
                        },{
                            value: 10,
                        }
                    ]
                    
                },{
                    type: "matrix",
                    name: "note sa mere",
                    title: "Si tu devais évaluer la qualité de ta relation avec ta mère entre 0 et 10,\
                             0 étant catastrophique et 10 étant excellente, quelle note mettrais-tu à cette relation ?",
                    columns: [
                        {
                            value: 0,
                        }, {
                            value: 1,
                        }, {
                            value: 3,
                        }, {
                            value: 4,
                        }, {
                            value: 5,
                        },{
                            value: 6,
                        },{
                            value: 7,
                        },{
                            value: 8,
                        },{
                            value: 9,
                        },{
                            value: 10,
                        }
                    ]
                    
                }
            ]///*******************************************************PAGE 5 Situation parents
        },{
            questions: [
                {
                    type: "radiogroup",
                    name: "Situation parents",
                    hasOther: true,
                    title: "À ce moment-là, tes parents sont ?",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 4,
                    choices: ["séparés","divorcés","se disputent","tout va bien"]
                },{
                    type: "matrix",
                    name: "Evalue relation parents",
                    title: "Si tu devais évaluer la qualité de leur relation entre 0 et 10, \
                            0 étant catastrophique et 10 étant excellente, quelle note mettrais-tu à la relation de tes parents ? ",
                    columns: [
                        {
                            value: 0,
                        }, {
                            value: 1,
                        }, {
                            value: 3,
                        }, {
                            value: 4,
                        }, {
                            value: 5,
                        },{
                            value: 6,
                        },{
                            value: 7,
                        },{
                            value: 8,
                        },{
                            value: 9,
                        },{
                            value: 10,
                        }
                    ]
                    
                }
            ]///*****************GRAND PARENTS***********************PAGE 6*************************
        },{
            questions: [
                {
                    type: "matrix",
                    name: "grandsparents",
                    title: "Tes grands-parents étaient…",
                    columns: [
                        {
                            value: "vivant(e)",
                            text:  "vivant(e)"
                        }, {
                            value: "mort(e)",
                            text:  "mort(e)"
                        }, {
                            value: "malade",
                            text:  "malade"
                        }
                    ],
                    rows: [
                        {
                            value: "Grand-père maternel",
                            text:  "Grand-père maternel"
                        }, {
                            value: "Grand-mère maternelle",
                            text:  "Grand-mère maternelle"
                        }, {
                            value: "Grand-père paternel",
                            text:  "Grand-père paternel"
                        }, {
                            value: "Grand-mère paternelle",
                            text:  "Grand-mère paternelle"
                        }
                    ]
                },{
                    type: "radiogroup",
                    name: " en paix avec leur maladie/mort",
                    title: "Es-tu en paix avec leur maladie / leur mort ? ",
                    visibleIf: "rowsWithValue({grandsparents}, 'malade') >= 1 or rowsWithValue({grandsparents}, 'mort(e)') >= 1",
                    isRequired: true,
                    choices: ["oui","non"]
                }//More questions here to complete this nonsens

            ]///*****************Ta famille*PAGE 7 freres/**************** */
        },
        {
            questions: [
                {
                    type: "radiogroup",
                    name: "possede freres",
                    title: "As-tu des frères ?",
                    isRequired: true,
                    choices: ["Oui", "Non"],
                    colCount: 0
                }, {
                    type: "dropdown",
                    name: "freres",
                    title: "Combien de frères?",
                    visibleIf: "{possede freres}='Oui'",
                    isRequired: true,
                    choices: [1, 2, 3, 4, 5],
                     colCount: 0
                }, {
                    type: "text",
                    name: "Nom_frère1",
                    title:"Nom frère 1",
                    inputType: "text",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 1",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                },{
                    type: "text",
                    name: "Nom_frère2",
                    title:"Nom frère 2",
                    inputType: "text",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 2",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                },{
                    type: "text",
                    name: "Nom_frère3",
                    title:"Nom frère 3",
                    inputType: "text",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 3",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                },{
                    type: "text",
                    name: "Nom_frère4",
                    title:"Nom frère 4",
                    inputType: "text",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 4",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                },{
                    type: "text",
                    name: "Nom_frère5",
                    title:"Nom frère 5",
                    inputType: "text",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 5",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                }, {
                    type: "dropdown",
                    name: "relationfrere1",
                    title: "Évaluer la qualité de ta relation avec ton frère:",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 1",
                    isRequired: true,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }, {
                    type: "dropdown",
                    name: "relationfrere2",
                    title: "Évaluer la qualité de ta relation avec ton frère:",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 2",
                    isRequired: true,
                    startWithNewLine: false,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }, {
                    type: "dropdown",
                    name: "relationfrere3",
                    title: "Évaluer la qualité de ta relation avec ton frère:",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 3",
                    isRequired: true,
                    startWithNewLine: false,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }, {
                    type: "dropdown",
                    name: "relationfrere4",
                    title: "Évaluer la qualité de ta relation avec ton frère:",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 4",
                    isRequired: true,
                    startWithNewLine: false,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }, {
                    type: "dropdown",
                    name: "relationfrere5",
                    title: "Évaluer la qualité de ta relation avec ton frère:",
                    visibleIf: "{possede freres}='Oui' and {freres} >= 5",
                    isRequired: true,
                    startWithNewLine: false,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }
                    
            ]/////***********************************************page 8 LINE sœurs
        },{
            questions: [
                {
                    type: "radiogroup",
                    name: "possede sœurs",
                    title: "As-tu des sœurs ?",
                    isRequired: true,
                    choices: ["Oui", "Non"],
                    colCount: 0
                }, {
                    type: "dropdown",
                    name: "sœurs",
                    title: "Combien de sœurs?",
                    visibleIf: "{possede sœurs}='Oui'",
                    isRequired: true,
                    choices: [1, 2, 3, 4, 5],
                     colCount: 0
                }, {
                    type: "text",
                    name: "Nom_sœurs1",
                    title:"Nom sœurs 1",
                    inputType: "text",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 1",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                },{
                    type: "text",
                    name: "Nom_sœurs2",
                    title:"Nom sœurs 2",
                    inputType: "text",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 2",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                },{
                    type: "text",
                    name: "Nom_sœurs3",
                    title:"Nom sœurs 3",
                    inputType: "text",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 3",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                },{
                    type: "text",
                    name: "Nom_sœurs4",
                    title:"Nom sœurs 4",
                    inputType: "text",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 4",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                },{
                    type: "text",
                    name: "Nom_sœurs5",
                    title:"Nom sœurs 5",
                    inputType: "text",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 5",
                    isRequired: true,
                    startWithNewLine: false,
                    colCount: 0
                }, {
                    type: "dropdown",
                    name: "relationsœurs1",
                    title: "Évaluer la qualité de ta relation avec ta sœurs:",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 1",
                    isRequired: true,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }, {
                    type: "dropdown",
                    name: "relationsœurs2",
                    title: "Évaluer la qualité de ta relation avec ta sœurs",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 2",
                    isRequired: true,
                    startWithNewLine: false,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }, {
                    type: "dropdown",
                    name: "relationsœurs3",
                    title: "Évaluer la qualité de ta relation avec ta sœurs:",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 3",
                    isRequired: true,
                    startWithNewLine: false,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }, {
                    type: "dropdown",
                    name: "relationfrere4",
                    title: "Évaluer la qualité de ta relation avec ta sœurs:",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 4",
                    isRequired: true,
                    startWithNewLine: false,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }, {
                    type: "dropdown",
                    name: "relationfrere5",
                    title: "Évaluer la qualité de ta relation avec ta sœurs:",
                    visibleIf: "{possede sœurs}='Oui' and {sœurs} >= 5",
                    isRequired: true,
                    startWithNewLine: false,
                    choices: [
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10
                    ]
                }
                    
            ]/////***********************************************page 9 enfants
        },{
            questions: [
                {
                    type: "radiogroup",
                    name: "possede enfants",
                    title: "As-tu des enfants ?",
                    isRequired: true,
                    choices: ["Oui", "Non"],
                    colCount: 0
                },{
                    type: "radiogroup",
                    name: "En paix avec cela",
                    title: "Si non, es-tu en paix avec le fait de ne pas en avoir ? ",
                    visibleIf: "{possede enfants} = 'Non'",
                    isRequired: true,
                    colCount: 0,
                    choices: ["Non      ",
                              "Pluôt non",
                              "Pluôt oui",
                              "Oui      "]
                },{
                    type: "radiogroup",
                    name: "Enfant grossesse",
                    title: "Y a-t-il eu d’autres grossesses ? (avortement, fausses couches, pertes…) ",
                    visibleIf: "{possede enfants} = 'Oui'",
                    isRequired: true,
                    colCount: 0,
                    choices: ["Oui","Non"]
                },{
                    type: "radiogroup",
                    name: "En paix avec cela1",
                    title: "Si oui, es-tu en paix avec le fait de ne pas en avoir ? ",
                    visibleIf: "{Enfant grossesse} = 'Oui'",
                    isRequired: true,
                    colCount: 0,
                    choices: ["Non      ",
                              "Pluôt non",
                              "Pluôt oui",
                              "Oui      "]
                }
            ]

        }
    ]//********************************:LINE 93 */
   };

   
window.survey = new Survey.Model(json);
survey
    .onComplete
    .add(function (result) {
        document
            .querySelector('#surveyResult')
            save("result", JSON.stringify(result.data));
          //  .innerHTML = "result: " + JSON.stringify(result.data);
    });

$("#surveyElement").Survey({model: survey});







